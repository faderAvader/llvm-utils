//
// Created by fader on 02.06.16.
//

#pragma once

#include <llvm/Support/raw_ostream.h>

namespace utils {
namespace io {

using namespace llvm;

inline void write(raw_ostream& buf) {
}
template<typename T, typename... Ts>
void write(raw_ostream& buf, T&& t, Ts&&... ts) {
  buf << t;
  write(buf, std::forward<Ts>(ts)...);
}

} // end namespace io
} // end namespace utils