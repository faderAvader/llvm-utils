// This file is distributed under the Modified BSD Open Source License.
// See LICENSE.TXT for details.

#pragma once

#include <llvm/ADT/SmallString.h>
#include <llvm/Support/raw_ostream.h>
#include <string>

namespace utils {
namespace io {

using namespace llvm;

/// A string that writes to a std::string.
/// The target string is owned by the stream to ease some simple workflows.
struct StringStream : raw_string_ostream {
  StringStream() : raw_string_ostream{txt} {}
private:
  std::string txt;
};

/// A string that writes to a SmallString.
/// The target string is owned by the stream to ease some simple workflows.
template<size_t SmallSize>
struct SmallStringStream : raw_svector_ostream{
  SmallStringStream() : raw_svector_ostream{txt} {}
private:
  SmallString<SmallSize> txt;
};

} // end namespace io
} // end namespace utils
