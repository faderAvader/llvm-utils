// This file is distributed under the Modified BSD Open Source License.
// See LICENSE.TXT for details.

#pragma once

#include <llvm/ADT/StringRef.h>

namespace llvm {
  class raw_ostream;
}

namespace utils {
namespace io {

using namespace llvm;

/// Helper for printing comma separated lists.
/// The first time it is sent to an raw_ostream, nothing is actually printed.
///
/// @code{.cpp}
///   Separator sep{", "};
///   for (auto item : items)
///     OS << sep << item; // will not print anything the first time.
/// @endcode
struct Separator {
  Separator() : Separator{", "} {}
  explicit Separator(StringRef sep) : Separator{"", sep} {}
  Separator(StringRef first, StringRef sep) : _first{first}, _separator{sep}, _at_first{true} {}

  StringRef str() {
    if (_at_first) {
      _at_first = false;
      return _first;
    } else {
      return _separator;
    }
  }
private:
  StringRef _first, _separator;
  bool _at_first;
};

raw_ostream& operator<<(raw_ostream& OS, Separator& sep);

} // end namespace io
} // end namespace utils
