// This file is distributed under the Modified BSD Open Source License.
// See LICENSE.TXT for details.

#include <llvm-utils/adt.hpp>

using namespace utils;
using namespace utils::adt;
using namespace llvm;
