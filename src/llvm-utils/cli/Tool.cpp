//
// Created by fader on 22/01/16.
//

#include <llvm-utils/cli/Tool.hpp>
#include <llvm/Support/CommandLine.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/Support/ManagedStatic.h>
#include <llvm/Support/PrettyStackTrace.h>

using namespace utils;
using namespace utils::cli;
using namespace llvm;

//**********************************************************************************************************************
//**** Tool

Tool::Tool(LLVMContext& ctx, int argc, const char *const* argv)
: _ctx{ctx}
, _argc{argc}
, _argv{argv}
{}

void Tool::init() {
  assert(!is_initialized() && "Tried to initialize tool twice");

  preInit();

  cl::AddExtraVersionPrinter(TargetRegistry::printRegisteredTargetsForVersion);
  cl::ParseCommandLineOptions(_argc, _argv);

  postInit();

  assert(!is_initialized());
  _initialized = true;
}

