//
// Created by fader on 22.01.16.
//

#pragma once

#include <llvm-utils/cli/Tool.hpp>
#include <llvm/Target/TargetOptions.h>
#include <llvm/Target/TargetSubtargetInfo.h>
#include <llvm/Target/TargetRegisterInfo.h>
#include <llvm/Support/CommandLine.h>

namespace utils {
namespace cli {

using namespace llvm;

extern cl::OptionCategory MachineConfigCategory;
extern cl::opt<std::string> TargetTriple;

/// Helper class for writing command line tools that use LLVMs backend infrastructure.
struct MachineCodeTool : Tool {
  MachineCodeTool(LLVMContext &ctx, int argc, const char* const *argv);

  static void run[[noreturn]](int argc, const char *const* argv, std::function<void(const MachineCodeTool&)> main) {
    generic_run<MachineCodeTool>(argc, argv, main);
  }

  const TargetMachine& getTargetMachine() const {
    assert(_target_machine);
    return *_target_machine;
  }

  const TargetSubtargetInfo& getSubTarget() const {
    assert(_subtargetInfo);
    return *_subtargetInfo;
  }

  const TargetRegisterInfo& getRegisters() const {
    const auto* regs = getSubTarget().getRegisterInfo();
    assert(regs);
    return *regs;
  }

  const MCAsmInfo& getAsmInfo() const {
    assert(_mcAsmInfo);
    return *_mcAsmInfo;
  }

  const Target& getTarget() const {
    return getTargetMachine().getTarget();
  }

  const Triple& getTargetTriple() const {
    return getTargetMachine().getTargetTriple();
  }
  const TargetOptions& getTargetOptions() const {
    return getTargetMachine().Options;
  }
protected:
  void preInit()  override;
  void postInit() override;
private:
  const TargetSubtargetInfo* makeSubTarget(
    const TargetMachine& machine,
    StringRef cpu,
    StringRef featureString,
    FloatABI::ABIType floatABI
  );

  std::unique_ptr<TargetMachine> _target_machine;
  const TargetSubtargetInfo*     _subtargetInfo = nullptr;
  std::unique_ptr<MCAsmInfo>     _mcAsmInfo;
};

} // end namespace cli
} // end namespace utils