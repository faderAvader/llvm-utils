//
// Created by fader on 02.06.16.
//

#pragma once

#include <llvm-utils/io/misc.hpp>
#include <llvm/ADT/StringRef.h>
#include <llvm/ADT/SmallString.h>
#include <llvm/ADT/Twine.h>
#include <llvm/Support/raw_ostream.h>

namespace utils {
namespace cli {

using namespace llvm;

struct Diagnostic {
  Diagnostic() : _toolOS{_tool}, _locationOS{_location}, _messageOS{_message} {}

  StringRef tool_str() {
    return _tool.str();
  }
  StringRef location_str() {
    return _location.str();
  }
  StringRef message_str() {
    return _message.str();
  }

  raw_svector_ostream& tool()     { return _toolOS; }
  raw_svector_ostream& location() { return _locationOS; }
  raw_svector_ostream& message()  { return _messageOS; }

  void print(raw_ostream& OS);
private:
  SmallString<64> _tool, _location, _message;
  raw_svector_ostream _toolOS, _locationOS, _messageOS;
};

/// report error and kill program
void reportError[[noreturn]](Diagnostic&);

template<typename... Ts>
void reportError[[noreturn]](const Twine& progname, const Twine& location, Ts&&... ts) {
  Diagnostic diag;

  diag.tool()     << progname;
  diag.location() << location;

  io::write(diag.message(), std::forward<Ts>(ts)...);
  reportError(diag);
}

/// report error
void reportWarning(Diagnostic&);

template<typename... Ts>
void reportWarning(const Twine& progname, const Twine& location, Ts&&... ts) {
  Diagnostic diag;

  diag.tool()     << progname;
  diag.location() << location;

  io::write(diag.message(), std::forward<Ts>(ts)...);
  reportWarning(diag);
}

} // end namespace cli
} // end namespace utils