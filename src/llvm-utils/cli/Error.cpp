//
// Created by fader on 02.06.16.
//

#include <llvm-utils/cli/Error.hpp>
#include <assert.h>                    // for assert
#include <llvm/Support/raw_ostream.h>  // for errs, raw_ostream
#include <stdlib.h>                    // for exit
#include <string>                      // for basic_string, operator==

using namespace utils::cli;
using namespace llvm;

static void report(raw_ostream::Colors color, StringRef type, Diagnostic& diagnostic) {
  auto& OS = errs();

  StringRef tool     = diagnostic.tool_str();
  StringRef location = diagnostic.location_str();
  StringRef msg      = diagnostic.message_str();

  if (tool.size()) {
    OS.changeColor(color, true);
    OS << tool << ": ";
  }

  if (location.size()) {
    OS.changeColor(color, true);
    OS << location << ": ";
  }

  assert(msg.size());
  OS.resetColor();
  OS.changeColor(raw_ostream::SAVEDCOLOR, true);
  OS << msg;
  OS << "\n";
  OS.resetColor();
  OS.flush();
}

void utils::cli::reportWarning(Diagnostic& diagnostic) {
  report(raw_ostream::MAGENTA, "warning", diagnostic);
}

void utils::cli::reportError(Diagnostic& diagnostic) {
  report(raw_ostream::RED, "error", diagnostic);
  exit(1);
}

