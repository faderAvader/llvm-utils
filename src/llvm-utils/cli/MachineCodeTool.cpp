//
// Created by fader on 22.01.16.
//

#include <llvm-utils/cli/MachineCodeTool.hpp>
#include <llvm/IR/LLVMContext.h>
#include <llvm/MC/SubtargetFeature.h>
#include <llvm/Target/TargetRegisterInfo.h>
#include <llvm/Target/TargetSubtargetInfo.h>
#include <llvm/Support/CommandLine.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/TargetSelect.h>

/// ! THIS HEADER *DEFINES* ALL ITS SYMBOLS !
/// You can only include it in one cpp file (*never* in *any* header!)
/// We can't even write our own forward declarations since it contains lots of static inline functions.
/// Who wrote this abomination!?!
#include <llvm/CodeGen/CommandFlags.h>

using namespace utils;
using namespace cli;
using namespace llvm;

cl::OptionCategory utils::cli::MachineConfigCategory{
  "machine config",
  "configure what type of machine we target (CPU, OS, ...)"
};

cl::opt<std::string> utils::cli::TargetTriple(
  "mtriple",
  cl::desc("Override target triple for module"),
  cl::cat{MachineConfigCategory}
);


//**********************************************************************************************************************
//**** MachineCodeTool

MachineCodeTool::MachineCodeTool(LLVMContext &ctx, int argc, const char *const *argv) : Tool{ctx, argc, argv} {
}

void MachineCodeTool::preInit() {
  Tool::preInit();

  // this must happen before argument parsing so the version printer can show supported targets.
  llvm::InitializeAllTargets();
  llvm::InitializeAllTargetInfos();
  llvm::InitializeAllTargetMCs();
  llvm::InitializeAllAsmPrinters();
  llvm::InitializeAllAsmParsers();
  llvm::InitializeAllDisassemblers();
}

void MachineCodeTool::postInit() {
  Tool::postInit();

  /// Figure out the target triple.
  Triple target_triple{Triple::normalize(TargetTriple.empty() ? sys::getDefaultTargetTriple() : TargetTriple)};

  // Get the target specific parser.
  std::string Error;
  const Target *target = TargetRegistry::lookupTarget("", target_triple, Error);
  if (!target)
    error("could not initialize target: ", Error);

  // Package up features to be passed to target/subtarget
  const std::string FeaturesStr = [&](){
    SubtargetFeatures Features;

    if (MAttrs.size()) {
      for (unsigned i = 0; i != MAttrs.size(); ++i)
        Features.AddFeature(MAttrs[i]);
    }

    return Features.getString();
  }();

  const CodeGenOpt::Level OLvl = CodeGenOpt::Default;

  TargetOptions Options                 = InitTargetOptionsFromCodeGenFlags();
  Options.DisableIntegratedAS           = false; // Enable integrated assembler
  Options.MCOptions.ShowMCEncoding      = true;  // Show encoding in .s output
  Options.MCOptions.MCUseDwarfDirectory = true;  // Use .file directives with an explicit directory
  Options.MCOptions.AsmVerbose          = true;  // Add comments to directives.

  const StringRef cpu_name = [&]() -> StringRef {
    if (!MCPU.empty())
      return MCPU.getValue();

    // TODO: For now we use one canonical CPU model for a given Triple::ArchType.
    //       We try use a quite modern CPU in order to enable as many features as possible.
    //       This is a hack to work around test failures on the CI server (due to missing CPU features).
    //       Once the sacred TargetTuple finally comes along we can directly use that and avoid the whole disgusting
    //       target string business.
    switch (target_triple.getArch()) {
      case Triple::aarch64:    // AArch64 (little endian): aarch64
      case Triple::aarch64_be: // AArch64 (big endian): aarch64_be
        return "cortex-a57";
      case Triple::x86:        // X86: i[3-9]86
        return "yonah";
      case Triple::x86_64:     // X86-64: amd64, x86_64
        return "skylake";
      default:
        error("Unsupported CPU arch: `", target_triple.getArchName() + "'");
    }
  }();

  _target_machine.reset(
      target->createTargetMachine(
          target_triple.getTriple(),
          cpu_name,
          FeaturesStr,
          Options,
          RelocModel.getValue(),
          CMModel,
          OLvl
      )
  );
  if (!_target_machine)
    error("Could not create LLVM target machine!", target_triple.getTriple());

  _subtargetInfo = makeSubTarget(*_target_machine, cpu_name, StringRef{FeaturesStr}, FloatABIForCalls);
  if (!_subtargetInfo)
    error("no subtarget info for target '", target_triple.getTriple());

  _mcAsmInfo.reset(target->createMCAsmInfo(
    *_subtargetInfo->getRegisterInfo(),
    _target_machine->getTargetTriple().getTriple()
  ));
  assert(_mcAsmInfo && "Error creating MCAsmInfo");
}

const TargetSubtargetInfo* MachineCodeTool::makeSubTarget(const TargetMachine& machine, StringRef cpu, StringRef featureString, FloatABI::ABIType floatABI) {
  // TODO: bool mips16
  // TODO: bool nomips16

  auto* dummy = Function::Create(FunctionType::get(Type::getVoidTy(ctx()), false), GlobalValue::InternalLinkage);

  dummy->addFnAttr("target-cpu", cpu);
  dummy->addFnAttr("target-features", featureString);
  switch (floatABI) {
    case FloatABI::Soft:
      dummy->addFnAttr("use-soft-float", "true");
      break;
    case FloatABI::Hard:
      dummy->addFnAttr("use-soft-float", "false");
      break;
    default:
      break;
  }

  auto* tgt = machine.getSubtargetImpl(*dummy);

  delete dummy;

  return tgt;
}
