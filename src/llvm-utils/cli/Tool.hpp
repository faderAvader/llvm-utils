//
// Created by fader on 22/01/16.
//

#pragma once

#include <llvm-utils/cli/Error.hpp>
#include <cassert>
#include <llvm/Target/TargetMachine.h>
#include <llvm/MC/MCAsmInfo.h>
#include <llvm/MC/MCRegisterInfo.h>
#include <llvm/ADT/iterator_range.h>
#include <llvm/ADT/iterator_range.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/Support/PrettyStackTrace.h>
#include <llvm/Support/ManagedStatic.h>

namespace llvm {
  class TargetSubtargetInfo;
}

namespace utils {
namespace cli {

using namespace llvm;

/// Helper class for writing a CLI tool that uses LLVM.
/// Initializes basic LLVM infrastructure, but oes not initialize targets or anything backend related.
struct Tool {
  Tool(LLVMContext& ctx, int argc, const char *const* argv);

  /// Create an instance of Tool and run a `main' function with it as parameter, then exit(0)
  static void run[[noreturn]](int argc, const char *const* argv, std::function<void(const Tool&)> main) {
    generic_run<Tool>(argc, argv, main);
  }

  void init();

  StringRef program_name() const { return _argv[0]; }

  LLVMContext&       ctx()       { return _ctx; }
  const LLVMContext& ctx() const { return _ctx; }

  int                 argc() const { return _argc; }
  const char * const* argv() const { return _argv; }

  iterator_range<const char * const*> args() {
    return make_range(_argv, _argv + _argc);
  }

  bool is_initialized() const { return _initialized; }

  template<typename... Args>
  void error[[noreturn]](const Args&... args) const {
    errorAt("", args...);
  }

  template<typename... Args>
  void warning[[noreturn]](const Args&... args) const {
    warningAt("", args...);
  }

  template<typename... Args>
  void errorAt[[noreturn]](const Twine& location, const Args&... args) const {
    reportError(program_name(), location, args...);
  }

  template<typename... Args>
  void warningAt[[noreturn]](const Twine& location, const Args&... args) const {
    reportWarning(program_name(), location, args...);
  }
protected:
  /// Called before command line arguments are parsed
  virtual void preInit()  {}

  /// Called after command line arguments are parsed
  virtual void postInit() {}

  /// Create an instance of a Tool subclass and run a `main' function with it as parameter.
  template<typename ToolT, typename Main>
  static void generic_run[[noreturn]](int argc, const char *const* argv, Main&& main) {
    static_assert(std::is_base_of<Tool, ToolT>::value, "Can only run subtypes of Tool");

    PrettyStackTraceProgram stackTrace{argc, argv};
    llvm_shutdown_obj shutdown;
    LLVMContext ctx;

    ToolT tool{ctx, argc, argv};

    tool.init();

    main(tool);
    exit(0);
  }
private:
  Tool(Tool&&) = delete;
  Tool(const Tool&) = delete;

  LLVMContext& _ctx;
  int _argc;
  const char *const* _argv;
  bool _initialized = false;
};

} // end namespace cli
} // end namespace utils