// This file is distributed under the Modified BSD Open Source License.
// See LICENSE.TXT for details.

#include <llvm-utils/io.hpp>
#include <llvm/Support/raw_ostream.h>

using namespace utils;
using namespace utils::io;
using namespace llvm;

raw_ostream& utils::io::operator<<(raw_ostream& OS, Separator& sep) {
  return OS << sep.str();
}
