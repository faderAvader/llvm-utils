//
// Created by fader on 21.03.16.
//

//****************************************************************************//
// Helper classes & functions for working with llvm::GraphTraits and related
// algorithms.
//****************************************************************************//

#pragma once

#include <llvm/ADT/GraphTraits.h>
#include <cstddef>

namespace utils {
namespace adt {

/// Wrapper around a normal STL iterator for use with llvm::GraphTraits.
/// We need this helper class because LLVMs GraphTraits requires an implicit conversion from
/// iterator to iterator::value_type
template<typename It>
struct GraphTraitsIterator final {
  using Iterator          = It;
  using iterator_category = typename It::iterator_category;
  using value_type        = typename It::value_type;
  using difference_type   = typename It::difference_type;
  using pointer           = typename It::pointer;
  using reference         = typename It::reference;

  GraphTraitsIterator() = default;

  explicit GraphTraitsIterator(Iterator it) : _it{it} { }

  GraphTraitsIterator &operator+=(size_t dist) {
    _it += dist;
    return *this;
  }

  GraphTraitsIterator &operator++() {
    ++_it;
    return *this;
  }

  GraphTraitsIterator operator++(int postfix) {
    GraphTraitsIterator it{_it};
    ++(*this);
    return it;
  }

  reference operator*() {
    return *_it;
  }

  operator value_type() { return *(*this); }

  bool operator==(GraphTraitsIterator that) const {
    return _it == that._it;
  }

  bool operator!=(GraphTraitsIterator that) const {
    return _it != that._it;
  }

  friend difference_type operator-(GraphTraitsIterator a, GraphTraitsIterator b) {
    return a._it - b._it;
  }

private:
  Iterator _it;
};

} // end namespace adt
} // end namespace utils
