// This file is distributed under the Modified BSD Open Source License.
// See LICENSE.TXT for details.

#pragma once

#include <cassert>
#include <utility>
#include <iterator>

namespace utils {
namespace adt {

/// A very simple range of iterators.
template<typename It>
struct Range {
  Range(It begin, It end) : _begin{begin}, _end{end} {}

  It begin() const { return _begin; }
  It end()   const { return _end;   }

  bool empty() const { return _begin == _end; }

  auto size() const {
    typename std::iterator_traits<It>::difference_type dist = _end - _begin;
    return dist;
  }

  auto front() {
    assert(!this->empty());

    return *_begin;
  }

  Range pop_front() const {
    assert(!this->empty());

    return Range{_begin + 1, _end};
  }

  Range pop_back() const {
    assert(!this->empty());

    return Range{_begin, _end - 1};
  }
private:
  It _begin, _end;
};

/// \brief Convenience function for iterating over sub-ranges.
///
/// This provides a bit of syntactic sugar to make using sub-ranges
/// in for loops a bit easier. Analogous to std::make_pair().
template <class T> Range<T> make_range(T x, T y) {
  return Range<T>(std::move(x), std::move(y));
}

template <typename T> Range<T> make_range(std::pair<T, T> p) {
  return Range<T>(std::move(p.first), std::move(p.second));
}

} // end namespace adt
} // end namespace utils
