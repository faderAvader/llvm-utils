
= llvm-utils =

A collection of small utility libraries for use with LLVM related projects.

We try to avoid forcing users to link to LLVM itself (except maybe libLLVMCore and libLLVMSupport), but make heavy use of ADT and other header only libraries.


