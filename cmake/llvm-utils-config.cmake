# This file is distributed under the Modified BSD Open Source License.
# See LICENSE.TXT for details.

cmake_minimum_required(VERSION 3.1)

set(llvm-utils_CMAKE_DIR "${llvm-utils_DIR}")
