// This file is distributed under the Modified BSD Open Source License.
// See LICENSE.TXT for details.

/// A simple file with an empty main that just checks if our header only libraries compile.

#include <llvm-utils/utils.hpp>

using namespace utils;
using namespace utils::adt;
using namespace llvm;

auto test_range() {
  std::map<int,bool> m;
  auto range = utils::adt::make_range(m.begin(), m.end());
  return range;
}

int main() {}
