//
// Created by fader on 02.06.16.
//

#include <llvm-utils/cli/MachineCodeTool.hpp>

using namespace utils::cli;

int main(int argc, const char*const* argv) {
  MachineCodeTool::run(argc, argv, [](const MachineCodeTool&) {
  });
}