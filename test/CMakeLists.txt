# This file is distributed under the Modified BSD Open Source License.
# See LICENSE.TXT for details.

link_libraries(llvmutils)

add_executable(test-header-only test-header-only.cpp)

add_simple_test(test-empty-tool)
add_simple_test(test-empty-mctool)
